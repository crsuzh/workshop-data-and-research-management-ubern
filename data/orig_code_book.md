# Code book for original study data

The data is available online from the [Harvard Dataverse](https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/27366) as the Lab Experiment. The data is licensed under a CC0 1.0. 

From the online supplement of the [original paper](https://www.science.org/doi/10.1126/science.1253932) we know the meaning of the different variables:

* `ID`: participant ID
* `low`: binary indicator [0,1] for 5% overhead condition
* `lowcover`: binary indicator [0,1] for 5% overhead, covered condition
* `high`: binary indicator [0,1] for 50% overhead condition
* `highcover`: binary indicator [0,1] for 50% overhead, covered condition
* `noover`: binary indicator [0,1] for no overhead control condition
* `allocation`: binary indicator [0,1] for allocated $100 to charity: water
* `donbeh`: count, [1 never to 6 or more times a year] for on average, how often do you donate money to nonprofits?
* `KKfamiliar`: scale [1 not at all to 7 very] for how familiar are you with Kids Korps?
* `CWfamiliar`: scale [1 not at all to 7 very] for how familiar are you with charity: water?
* `gender`: binary indicator [0,1] for gender with [1 = male]
* `age` age of participant

The five treatments are categorized by one of the following binary (1 vs. 0) variables: low (5% overhead), high (50% overhead), lowcover (5% covered overhead), highcover (50% covered overhead), and noover (no overhead as control).

There are a total of 449 observations (rows) in the data.
