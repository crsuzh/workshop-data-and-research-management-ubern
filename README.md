# Workshop Data and research management

This repository includes all material to reproduce the first results and Figure in Gneezy et al., and the test that was replicated as part of the Social Sciences Replication Project.

This repository contains:
- analysis
- data

Files are named according to the following convention: all lowercase with words seperated by underscores.


